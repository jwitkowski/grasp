<?php


add_theme_support('menus');




function register_my_menus() {
register_nav_menus(
array(
'main-nav' => __( 'Main Navigation' )
));
}

add_action( 'init', 'register_my_menus' );




//////////////////////////////////////////////////////////////
// Feature Images (Post Thumbnails)
/////////////////////////////////////////////////////////////

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' );
   set_post_thumbnail_size( 150, 150 );
   add_image_size( 'banner',  1000, 400, true ); 
    add_image_size( 'blog-thumbnail',  130, 85, true ); 
}
?>
