<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
   <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <base href="<?php bloginfo('template_directory'); ?>/" />


    <!-- Le styles -->
    
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/docs.css" rel="stylesheet">
    <link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:700,300' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>
   </head>
   <body>
        <div class="container">
            <header>
                <div class="row">
                    <div class="span5">
                        <img src="images/logo.png" alt="grasp"/>
                    </div><!--/span3-->
                
                <div class="span7">
                      <nav>
                          
          <div class="navbar navbar-inverse">
            <div class="navbar-inner">
             <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </a>

                <div class="nav-collapse collapse">
                  <?php wp_nav_menu(  array( 'theme_location' => 'main-nav', 'container_class' => '', 'container_id' => '', 'before' => '', 'after' => '', 'menu_class' =>'nav', 'menu_id' => '' ));  ?>
                </div><!--/nav-collapse-->
              </div><!--/navbar-inner-->
          </div><!--navbar-->
      </div><!--row-->
                </nav>
              </div>
            </header>