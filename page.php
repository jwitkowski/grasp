        <?php get_header(); ?>
          <div class="row feature" >
            <img src="images/banner.png" class="interior_feature"/>
            <div class="span5 interior_title">
                <h1> <?php the_title(); ?></h1>
              </div>
          </div><!--/row-->
          </div><!--/container-->
          <div class="content">
            <div class="container">
              <div class="row">
              <div class="span4 events">
                <h1>Upcoming Events</h1>
                <?php em_events('limit=1&format=<div class="eventHolder"><h3>#_EVENTNAME</h3>  Date: #M #j #Y<br/>Time: #_EVENTTIMES</div><a href="http://grasp-online.org/events/" class="button"> View Event</a>'); ?>
              </div><!--/span5 events-->
              <div class="span7 interior_paragraph">
                <?php while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
                <?php endwhile; // end of the loop. ?>
              </div>
            </div><!--/row-->
            </div><!--/container-->
          </div><!--/content-->
        <?php get_footer(); ?>
          