        <?php
        /**
         * Template Name: Home */
        get_header(); ?>
          <div class="row feature" >
            <div id="myCarousel" class="carousel fade">
              <!-- Carousel items -->
              <div class="carousel-inner">
                <div class="active item"><img src="images/photo1.jpg"/></div>
              </div><!--/ inner-->
              <div class="span5 header_title">
                <p>Meeting the <span>professional needs</span> of <span>School Psychologists</span> in the Rochester area.</p>
              </div>

              </div><!--/myCarousel-->
          </div><!--/row-->
          </div><!--/container-->
          <div class="content">
            <div class="container">
              <div class="row">
              <div class="span4 events">
                <h1>Upcoming Events</h1>
                <?php em_events('limit=1&format=<div class="eventHolder"><h3>#_EVENTNAME</h3>  Date: #M #j #Y<br/>Time: #_EVENTTIMES</div> <a href="http://grasp-online.org/events/" class="button"> View Event</a>'); ?>

              </div><!--/span5 events-->
              <div class="span7 home_paragraph">
                <h1>We are GRASP.</h1>
                Welcome to the website of the Greater Rochester Area School Psychologists, (GRASP). Our purpose is to develop workshops and conferences of interest to schoolpsychologists. GRASP has welcomed conference attendance from various disciplines. GRASP is neither affiliated with nor in competition with any other professional organization of school psychologists. By-Laws specifically preclude lobbying or other politically oriented activities. 
              </div>
            </div><!--/row-->
            </div><!--/container-->
          </div><!--/content--> 
        <?php get_footer(); ?>   
          